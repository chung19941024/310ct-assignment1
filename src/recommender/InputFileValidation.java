package recommender;

import java.io.File;
import java.nio.file.FileSystemException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InputFileValidation {
	private File file;

	private String type;
	private long timestamp;
	private boolean valid;

	public InputFileValidation(File file, String startTime) throws Exception {
		this.file = file;
		this.valid = true;
		
		try {
			if (!isValidFileLength()) {
				this.valid = false;
				throw new InputFileLengthException(file.getName());
			}
			if (!isValidFileExtension()) {
				this.valid = false;
				throw new InputFileExtensionException(file.getName());
			}
			if (!isValidFileType()) {
				this.valid = false;
				throw new InputFileTypeException(file.getName());
			}
			if (!isValidFileTimeStamp(file.getName().substring(2, 16))) {
				this.valid = false;
				throw new InputFileTimestampException(file.getName());
			}
			
			this.type = file.getName().substring(0, 2);
		} catch (Exception e) {
			System.out.println(currentTime() + e.getMessage());
		}
	}

	public String getType() {
		return this.type;
	}

	public long getTimestamp() {
		return this.timestamp;
	}

	private boolean isValidFileLength() {
		if (file.getName().length() != 20) {
			return false;
		}
		return true;
	}
	
	private String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }

	private boolean isValidFileExtension() {
		String fileExtension = getFileExtension(file);
		if (!fileExtension.equals("csv")) {
			return false;
		}
		return true;
	}

	private boolean isValidFileType() {
		String fileType = file.getName().substring(0, 2);
		if (!(fileType.equals("RR") || fileType.equals("UP"))) {
			this.valid = false;
			return false;
		}
		return true;
	}

	private boolean isValidFileTimeStamp(String s) {
		SimpleDateFormat format = new java.text.SimpleDateFormat("yyyyMMddhhmmss");
		try {
			Date date = format.parse(s);
			this.timestamp = (long) date.getTime() / 1000;
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public InputFile getFile() throws Exception {
		if (this.type.equals("RR")) {
			return new RRFile(this.file, this.type, this.timestamp);
		}
		if (this.type.equals("UP")) {
			return new UPFile(this.file, this.type, this.timestamp);
		}
		return null;
	}

	public boolean isValid() {
		return this.valid;
	}
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SS");

	public static String currentTime() {
		return sdf.format(new Timestamp(System.currentTimeMillis()));
	}

	class InputFileFormatException extends FileSystemException {
		public InputFileFormatException(String msg) {
			super(msg);
		}
	}

	class InputFileLengthException extends InputFileFormatException {
		public InputFileLengthException(String filename) {
			super(" File length invalid: " + filename);
		}
	}

	class InputFileTypeException extends InputFileFormatException {
		public InputFileTypeException(String filename) {
			super(" File type invalid: " + filename);
		}
	}

	class InputFileTimestampException extends InputFileFormatException {
		public InputFileTimestampException(String filename) {
			super(" File timestamp invalid: " + filename);
		}
	}

	class InputFileExtensionException extends InputFileFormatException {
		public InputFileExtensionException(String filename) {
			super(" File extension invalid: " + filename);
		}
	}
}

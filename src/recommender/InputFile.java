package recommender;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.Preference;

public class InputFile {
	private File file;
	private String type;
	private long timestamp;
	protected String processName = "File";

	private DataModel model;

	public InputFile(File file, String type, long timestamp) {
		this.file = file;
		this.type = type;
		this.timestamp = timestamp;
	}

	public File getFile() {
		return this.file;
	}

	public String getType() {
		return this.type;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public boolean process() throws Exception {
		try {
			if (this.type.equals("RR")) {
				processName = "Request processing";
			}
			if (this.type.equals("UP")) {
				processName = "Model updating";
			}
			
			
			System.out.println(currentTime() + " " + processName + " start: " + this.file.getName());
			if (FileEmpty(this.file)) {
				throw new FileEmptyException(this.file.getName());
			}
		} catch (Exception e) {
			System.out.println(currentTime() + e.getMessage());
			return false;
		} 
		return true;
	}

	private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SS");

	public static String currentTime() {
		return sdf.format(new Timestamp(System.currentTimeMillis()));
	}

	private boolean FileEmpty(File file) {
		return file.length() == 0;
	}
	
	protected boolean isNumeric(String strNum) {
		try {
			Double.parseDouble(strNum);
		} catch (NumberFormatException | NullPointerException nfe) {
			return false;
		}
		return true;
	}

	protected static void updateCurrentModelFile(DataModel model) throws IOException, TasteException {
		try {
			String content = "";
			for (Iterator i = model.getUserIDs(); i.hasNext();) {
				long userID = (long) i.next();
				for (Preference item : model.getPreferencesFromUser(userID)) {
					content += userID + "," + item.getItemID() + "," + item.getValue() + "\n";
				}
			}

			PrintWriter writer = new PrintWriter("processed/currentModel.csv");
			writer.print(content);
			writer.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	protected DataModel getModel() {
		return model;
	}

	protected void setModel(DataModel model) {
		this.model = model;
	}
	
	protected void deleteTmp() {
		new File("tmp.csv").delete();
	}

	protected void fileMoveToProcessed() throws IOException {
		try {
			Files.move(Paths.get(file.getPath()), Paths.get("processed/" + file.getName()));
		} catch (FileAlreadyExistsException e) {
			System.out.println(currentTime() + " " + e.getClass().getSimpleName() + " " + e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	class FileEmptyException extends IllegalArgumentException {
		public FileEmptyException(String filename) {
			super(" " + processName + " problem: " + filename + " - file is empty");
		}
	}

}
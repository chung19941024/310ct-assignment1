package recommender;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;

import org.apache.mahout.cf.taste.impl.model.GenericDataModel;
import org.apache.mahout.cf.taste.impl.model.GenericUserPreferenceArray;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.model.DataModel;

public class Recommender {

	public static void main(String[] args) throws Exception {
		try {
			createDirectory("input");
			createDirectory("output");
			createDirectory("processed");
			createTmpFile();
			Scanner scanner = new Scanner(System.in);
			System.out.println("Put files to <input> Directory, press 'Y' and press Enter");
			scanner.next().charAt(0);
			for (InputFile inputFile : prioritizeInputFiles()) { 
				inputFile.process();
			}
			new File("tmp.csv").delete();
			System.out.println(currentTime() + " program end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<InputFile> prioritizeInputFiles() throws Exception {
		ArrayList<InputFile> inputFiles = new ArrayList<InputFile>();

		for (File file : new File("input").listFiles()) {
			InputFileValidation inputFile = new InputFileValidation(file, currentTime());

			if (inputFile.isValid()) {
				inputFiles.add(inputFile.getFile());
			}
		}

		Collections.sort(inputFiles, new Comparator<InputFile>() {
			@Override
			public int compare(InputFile if1, InputFile if2) {
				return Long.valueOf(if1.getTimestamp()).compareTo(if2.getTimestamp());
			}
		});

		return inputFiles;
	}

	private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SS");

	public static String currentTime() {
		return sdf.format(new Timestamp(System.currentTimeMillis()));
	}

	public static void createDirectory(String dirName) {
		File inputDir = new File(dirName);

		if (!inputDir.exists()) {
			boolean result = false;

			try {
				inputDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				System.out.println(currentTime() + " directory create problem: " + se.getMessage());
			}
			if (result) {

			}
		}
	}

	public static void createTmpFile() throws IOException {
		File tmpFile = new File("tmp.csv");
		tmpFile.delete();
		File currentModelFile = new File("processed/currentModel.csv");
		if (currentModelFile.exists() && !currentModelFile.isDirectory()) {
			Files.copy(Paths.get(currentModelFile.getPath()), Paths.get(tmpFile.getPath()));
		} else {
			tmpFile.createNewFile();
			currentModelFile.createNewFile();
		}

	}
	
	public static void ListUserPreference(DataModel model) throws Exception {
		System.out.println("Num of Items: " + model.getNumItems());
		System.out.println("Num of Users: " + model.getNumUsers());
		
		for (Iterator i = model.getUserIDs(); i.hasNext();) {
			GenericUserPreferenceArray test = (GenericUserPreferenceArray) model.getPreferencesFromUser((long) i.next());
			System.out.println(test);
			
		}
	}
}

package recommender;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import org.apache.mahout.cf.taste.impl.model.GenericUserPreferenceArray;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.model.DataModel;

public class UPFile extends InputFile {
	public UPFile(File file, String type, Long timestamp) throws Exception {
		super(file, type, timestamp);
	}

	public boolean process() throws Exception {
		if (super.process()) {
			Scanner inputStream = null;
			try {
				inputStream = new Scanner(super.getFile());
				while (inputStream.hasNext()) {
					String line = inputStream.next();
					String[] values = line.split(",");
					if (!matchThreeColumn(values)) {
						throw new ColumnNotMatchException(super.getFile().getName());
					}
					if (!super.isNumeric(values[0]) || !super.isNumeric(values[1]) || !super.isNumeric(values[2])) {
						throw new ColumnNumberException(super.getFile().getName());
					}
					if (!PreferenceInRange(values[2])) {
						throw new preferenceValueException(super.getFile().getName());
					}
				}
				
				
				String content = getFileContent(super.getFile());
				appendToTmp(content);
				DataModel model = new FileDataModel(new File("tmp.csv"));
				super.setModel(model);
				updateCurrentModelFile(model);
				
				
				
				
				
				
				
				
			} catch (Exception e) {
				System.out.println(currentTime() + " "+ e.getMessage());
			} finally {
				inputStream.close();
			}
		}
		super.fileMoveToProcessed();
		System.out.println(currentTime() + " " + processName + " end: " + getFile().getName());
		return true;
	}

	private String getFileContent(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file.getAbsolutePath()));
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;
		String ls = System.getProperty("line.separator");
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}
		stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		reader.close();

		String content = stringBuilder.toString();
		return content;
	}

	private void appendToTmp(String content) throws IOException {
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			File file = new File("tmp.csv");
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file.getAbsoluteFile(), true);
			bw = new BufferedWriter(fw);
			bw.write(content);
		} catch (IOException e) {
			System.out.println(currentTime() + " " + processName + " problem: " + getFile().getName() + e.getMessage());
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				System.out.println(currentTime() + " " + processName + " problem: " + getFile().getName() + ex.getMessage());
			}
		}
	}

	private boolean PreferenceInRange(String preference) {
		Float value = Float.valueOf(preference);
		return value > 0 && value <= 5;
	}

	private boolean matchThreeColumn(String[] values) {
		if (values.length == 3) {
			return true;
		}
		return false;
	}
	
	

	class ColumnNotMatchException extends IllegalArgumentException {
		public ColumnNotMatchException(String filename) {
			super("Model updating problem: " + filename + " - column not match");
		}
	}

	class preferenceValueException extends NumberFormatException {
		public preferenceValueException(String filename) {
			super("Model updating problem: " + filename + " - preference not in range");
		}
	}

	class ColumnNumberException extends IllegalArgumentException {
		public ColumnNumberException(String filename) {
			super("Model updating problem: " + filename + " - column is not a number");
		}
	}
}
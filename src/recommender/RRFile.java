package recommender;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.Scanner;

import org.apache.mahout.cf.taste.common.NoSuchUserException;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.RMSRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import recommender.UPFile.ColumnNumberException;

public class RRFile extends InputFile {

	public RRFile(File file, String type, Long timestamp) throws Exception {
		super(file, type, timestamp);
	}

	public boolean process() throws Exception {
		Scanner inputStream = null;
		BufferedWriter writer = null;
		if (super.process()) {
			try {
				DataModel model = new FileDataModel(new File("tmp.csv"));
				super.setModel(model);

				String content = "";
				inputStream = new Scanner(super.getFile());
				while (inputStream.hasNext()) {
					content = inputStream.next();
					if (!super.isNumeric(content)) {
						throw new ColumnNumberException(super.getFile().getName());
					}
				}

				Long userID = Long.valueOf(content);

				RecommenderBuilder recommenderBuilder = new RecommenderBuilder() {
					public Recommender buildRecommender(DataModel model) throws TasteException {
						ItemSimilarity similarity = new PearsonCorrelationSimilarity(model);

						return new GenericItemBasedRecommender(model, similarity);
					}
				};

				Recommender recommender = recommenderBuilder.buildRecommender(model);

				List<RecommendedItem> recomendations = recommender.recommend(userID, 5);
				String result = String.valueOf(userID) + ",\"";
				int itemCount = 0;
				for (RecommendedItem recommendation : recomendations) {
					if (itemCount != 0) {
						result += ",";
					}
					result += recommendation.getItemID();
					itemCount++;
				}
				result += "\"";

				RecommenderEvaluator evaluator = new RMSRecommenderEvaluator();
				double score = evaluator.evaluate(recommenderBuilder, null, model, 0.7, 1.0);
				System.out.println("RMSE: " + score);

//				RecommenderIRStatsEvaluator statsEvaluator = new GenericRecommenderIRStatsEvaluator();
//				IRStatistics stats = statsEvaluator.evaluate(recommenderBuilder, null, model, null, 5, 4, 0.7);
//
//				System.out.println("Precision: " + stats.getPrecision());
//				System.out.println("Recall: " + stats.getRecall());
//				System.out.println("F1 Score: " + stats.getF1Measure());

				writer = new BufferedWriter(new FileWriter("output/" + super.getFile().getName()));
				writer.write(result);
				writer.close();

			} catch (NoSuchUserException e) {
				System.out.println(currentTime() + " Request processing problem: " + this.getFile().getName()
						+ " - no such user ID " + e.getMessage());
			} catch (ColumnNumberException e) {
				System.out.println(currentTime() + " " + e.getMessage());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.out.println(currentTime() + " Request processing problem: " + this.getFile().getName()
						+ " - data model no exist");
			} catch (Exception e) {
				System.out.println(currentTime() + " " + e.getMessage());
			} finally {
				inputStream.close();

			}
		}
		super.fileMoveToProcessed();
		System.out.println(currentTime() + " " + processName + " end: " + getFile().getName());
		return true;
	}

	private boolean FileEmpty(File file) {
		return file.length() == 0;
	}

	class ColumnNotMatchException extends IllegalArgumentException {
		public ColumnNotMatchException(String filename) {
			super("Request processing problem: " + filename + " - column not match");
		}
	}

	class ColumnNumberException extends IllegalArgumentException {
		public ColumnNumberException(String filename) {
			super("Request processing problem: " + filename + " - column is not a number");
		}
	}

}
